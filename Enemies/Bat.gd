extends KinematicBody2D

export var ACCELERATION = 300
export var MAX_SPEED = 50
export var FRICTION = 200
export var KNOCKBACK_SCALE = 10
export var WANDER_TARGET_DISTANCE = 0
export var INVINCIBILITY = 0.4

const EnemyDeathEffect = preload("res://Effects/EnemyDeathEffect.tscn")



enum {
	IDLE,
	WANDER,
	CHASE	
}

var knockback = Vector2.ZERO

var state = IDLE
var velocity = Vector2.ZERO

onready var stats = $Stats
onready var playerDetectionZone = $PlayerDetectionZone
onready var sprite = $AnimatedSprite
onready var hurtbox = $Hurtbox
onready var softCollision = $SoftCollision
onready var wanderController = $WanderController
onready var animationPlayer = $AnimationPlayer

func _ready():
	$AnimatedSprite.frame = randi() % $AnimatedSprite.frames.get_frame_count("Fly")
	state = pick_rand_state([IDLE, WANDER])
	
func _physics_process(delta):
	knockback = knockback.move_toward(Vector2.ZERO, FRICTION * delta)
	
	knockback = move_and_slide(knockback)

	match state:
		IDLE:
			idle_state(delta)
		WANDER:
			wander_state(delta)	
		CHASE:
			chase_state(delta)


	if softCollision.is_colliding():
		velocity += softCollision.get_push_vector() * delta * 400
		
	velocity = move_and_slide(velocity)

func restart_state_and_wander(states, r1, r2):
	state = pick_rand_state(states)
	wanderController.start_wander_timer(rand_range(r1, r2))
		
func idle_state(delta):
	velocity = velocity.move_toward(Vector2.ZERO,  FRICTION * delta)
	seek_player()
	
	if wanderController.get_time_left() == 0:
		restart_state_and_wander([IDLE, WANDER], 1, 3)
	
func moving_towards(p, delta):
	var direction = global_position.direction_to(p)
	velocity = velocity.move_toward(direction * MAX_SPEED, ACCELERATION * delta)

func wander_state(delta):
	seek_player()
	if wanderController.get_time_left() == 0:
		restart_state_and_wander([IDLE, WANDER], 1, 3)
	moving_towards(wanderController.target_position, delta)

	if global_position.distance_to(wanderController.target_position) <= WANDER_TARGET_DISTANCE:
		restart_state_and_wander([IDLE, WANDER], 1, 3)
		
	sprite.flip_h = velocity.x < 0
	
func chase_state(delta):
	var player = playerDetectionZone.player
	if player != null:
		moving_towards(player.global_position, delta)
	else:
		state = IDLE
		
	sprite.flip_h = velocity.x < 0
	
func seek_player():
	if playerDetectionZone.can_see_player():
		state = CHASE
	
func pick_rand_state(state_list):
	return state_list[randi() % state_list.size()]
	
func _on_Hurtbox_area_entered(area):
	stats.health -= area.damage
	
	knockback = (global_position - area.player.global_position) * KNOCKBACK_SCALE
	hurtbox.create_hit_effect()
	hurtbox.start_invincibility(INVINCIBILITY)
	
func _on_Stats_no_health():
	queue_free()
	var enemyDeathEffect = EnemyDeathEffect.instance()
	get_parent().add_child(enemyDeathEffect)
	enemyDeathEffect.global_position = global_position
	


func _on_Hurtbox_invincibility_started():
	animationPlayer.play("Start")


func _on_Hurtbox_invincibility_ended():
	animationPlayer.play("Stop")
