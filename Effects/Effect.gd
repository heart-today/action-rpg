extends AnimatedSprite


func _ready():
	var r = connect("animation_finished", self, "_on_animation_finished")
	assert(OK == r)
	frame = 0
	play("Animate")

func _on_animation_finished():
	queue_free()
