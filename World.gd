extends Node2D

onready var directions = $CanvasLayer/Directions

func _ready():
	directions.visible = true

func _physics_process(_delta):
	if Input.is_action_just_pressed("ui_focus_next"):
		directions.visible = not directions.visible

	if Input.is_action_just_pressed("ui_cancel"):
		var currentScene = get_tree().get_current_scene().get_filename()
		print(currentScene) # for Debug
		var result = get_tree().change_scene(currentScene)
		assert(result == OK)
		PlayerStats.reset()
