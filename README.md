# Action RPG

Based on [Godot Action RPG Series](https://www.youtube.com/playlist?list=PL9FzW-m48fn2SlrW0KoLT4n5egNdX-W9a)

See also [Tilesetter Tutorial](https://www.youtube.com/watch?v=SI3mZ3ynrTw)


## Demo

Hosted on [Their Temple](http://theirtemple.com/web/Action%20RPG/Action%20RPG.html)

- Click the screen to begin web game!
- WASD or Arrows to move
- J or X to attack
- Z or K to roll (only while moving!)
- Find the exit (NYI See #13 )

## Intentions

- To improve from the experience of [Heart Today Gone Tomato](https://hanumanjiyogi.itch.io/heart-today-gone-tomato) developed during [Godot Wild Jam 31](https://godotwildjam.com/)
- To develop a 2D game 
- To generate ideas for [Logos](https://gitlab.com/their-temple/logos) 
- To generate ideas for [Acro Avatars](https://gitlab.com/their-temple/acro-avatars)

## Features

- 2D terrain with walls
- Animated sprite running in 4 directions
- Attack and roll in 4 directions
- Enemy bats
- ...and much more!  (Just look at the titles of [Godot Action RPG Series](https://www.youtube.com/playlist?list=PL9FzW-m48fn2SlrW0KoLT4n5egNdX-W9a))

## Installation

It's a [Godot](https://godotengine.org) project using [Git](https://git-scm.com/downloads), so:
- Install [Godot](https://godotengine.org/download) 
- Install [Git](https://git-scm.com/downloads)
- Clone the project to your machine

  From a terminal:
  `> git clone https://gitlab.com/heart-today/action-rpg.git`
- Run [Godot](https://godotengine.org/download) and Import the project.godot file from the Godot Project Manager.

Binaries Windows, Linux or OSX can be provided by the asking!  ;-)

## License

